---
title: Creating Glue Code
publishdate: 2025-01-10
tags: [project, distributedrx]
---

# Creating Glue Code

## Setting up RabbitMQ

The first step for is creating a RabbitMQ instance. I will be using the [Docker image](https://hub.docker.com/_/rabbitmq/) for this and start it with

```shell
docker pull rabbitmq:3-management
docker run -d --hostname d10rx.local --name d10rx.local -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

Notice I'm using a version with management tools, so I can access the interface(http://localhost:15672, login with `guest, guest`) and exposing the ports on the host network manually (as `--network host` does not seem to work on Windows Docker wsl)

Then I'm starting with a slightly modified basic [C# sample](https://www.rabbitmq.com/tutorials/tutorial-one-dotnet.html) which can be found in this [gitlab repo](https://gitlab.com/gregorybleiker/rabbitmqstarter). The only difference is that I introduced commmand line switches for sender and receiver instead of having two separate files.

In order to make this pluggable and also to implement Rx operators later on, I want to replace this code

```csharp
channel.QueueDeclare(queue: "hello",
durable: false,
exclusive: false,
autoDelete: false,
arguments: null);

string message = "Hello World!";
var body = Encoding.UTF8.GetBytes(message);
channel.BasicPublish(exchange: "",
                     routingKey: "hello",
                     basicProperties: null,
                     body: body);
```

with something that is language agnostic. Enters [gRPC](https://grpc.io/). We will need to define a basic interface for a block. It will need to have a way to specify the queue name and a callback that it can call when it wants to send to the queue. gRPC tools will be installed with nuget and I have built a slightly modified version of the csharp sample from [here](https://github.com/grpc/grpc/blob/master/examples/csharp/RouteGuide)

Back to [Overview]({{< relref "2021-01-23-distributedrx.md" >}})
