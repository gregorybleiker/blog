---
title: Hierarchies are dead!
date: 2018-01-01
tags: [Introduction, graph, knowledge management]
---

## TL;DR

We need new tools to manage data. Check out this web-based knowledge management project at the [uberview gitlab project page](https://gitlab.com/gregorybleiker/uberview/)

## Motivation for Überview

We live in a world of data. We have file shares, we have online drives, we have sharepoint sites, we have the internet. And although the infrastructure is probably better than ever to host data, the way we work with data is far from satisfactory. It isn't a problem of hosting information, it is the problem of working with it. I'm sure you have encountered the situation where you are looking for a document and you ask your collegue for its location on a file share or drive. They might send you a link, or they might tell you that it is under the "usual location". What they mean is that there is a structure on your share that should help you navigate around. A typical structure could look something like this:

- Marketing
  - Internal
  - Customers
    - Customer A
      - Projects
      - ...
    - Customer B

And so on... The problem with this approach is that it puts a hierarchy on your data. On the first level, it might be organized the same as your company is, usually because it is simpler to manage the security and access rights. A level below it already begins to get fuzzy. Why the distinction between "Internal" and "Customers"? Some of the "Customers" data might be needed for internal purposes as well. The discussion on which level should contain what is similar to choosing colors for something. Nearly no one has a deeper knowledge of good design, but everybody has an oppinion on the subject (and some are quite adamant that they have right answer). I have also caught myself reorganizing my own data, because in the light of some new use case it might be more appropriate to organize the data differently, for example to put all the Customers on the top level (because after all, everything is customer related). Just to move it all back again a few months later because I thought the first use case was more important after all... **No matter how you do it, it is not going to suit all people or all needs.**

Up to now I have not met anyone who would really disagree with the reasoning above. Everyone knows its not optimal, but the wish for structure, for "being cleaned up" is stronger. After all, all the content will be searchable with the new shiny document management system (_that will be introduced next year..._ not). I have yet to see a company having a single DMS covering all documents and all departements, but that is not the topic here. The problem I want to address here is that you often are not only interested in finding a certain document. You are interested in finding everything related to a certain topic. That might include things that aren't documents! And even if the DMS coughs up a document where you somehow remembered a keyword that was in the document, you don't know if somewhere in an other folder lingers a document that is just as relevant to the topic you are interested in, but someone thought it was more "internal" than "customer-related".

Enter tags. Tags are word-like metadata you store together with your object. You might think that tags have been around a long time (and probably have not done much the help you with your data). **That is because no-one seems to doing it right**. Tags are something either extracted for you from a document or a few words you clap onto a document when you check it in to whatever system you are using that supports tags. While it is ok to have some tags auto-created and start with some individual tags, that should not be the end. It is also important to realize that tags might not be as universal as you think an in many cases rather subjective. I personally think that the concept of tags is highly underrated and deserves a better treatment.

Let's get back to our file share example. Instead of navigating through the folder structure to find something (let's say you are looking for a contract with a customer), wouldn't it be more convenient to start off by saying you need all documents that are contracts (for the moment, let's not worry how they got them, but they would have the "contracts" tag on them). Furthermore you could combine this with the "customer a" tag. Probably this will already be sufficient if you don't have hundreds of contracts with customer a, but let's say you really do have a lot of contracts with a customer. You will know something about the nature of the specific contract, and that would have to be... a tag. So maybe you are looking for a contract concerning a specific area of work, you maybe did some kind of upgrade, so you add the "upgrade" tag. You would then see everything in your system that satisfies all three tags, "contract", "customer a" and "upgrade".

It's now time to talk about where you see these items. This is the purpose uf Ueberview. It is a **web-based tool for working with your data in a visual way**. Tags and items are displayed as nodes of a graph. Überview stores metadata about your data and lets you work with it. It can link to your data, but it does not host it. As I pointed out earlier the infrastructure is not the problem any more. **It's more like a queriable mindmap on steroids.**

![Überview](/uview.png "uview")

Let's see what else it could do for you. With the documents found above (for "contract", "customer a" and "upgrade"), you might want to see what other upgrades you have done in the past. So you navigate from the "upgrade" tag to see all documents concerning an upgrade, and these might be combined with an other tag, for instance "technical documentation" (tags don't have to be single words, though it is advisable to keep them as short as possible). When you want to look at the real document, you click on the node that opens the link to your actual document.

In the next posts, I will examine other use cases for Überview, starting with a bookmark organizer.

You can check out the project at the [uberview gitlab page](https://gitlab.com/gregorybleiker/uberview/)!
