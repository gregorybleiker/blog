---
title: PureScript Journey Part 1
publishdate: 2022-03-05
tags: [vscode, functionalprogramming, purescript]
---

# Building a Web App in PureScript

In my personal quest to learn more about functional programming and following an online course on [Haskell](https://haskell.mooc.fi/), I will be writing down my steps on getting a web app in [PureScript](https://www.purescript.org/) running. The code to this project can be found [here](https://gitlab.com/gregorybleiker/pure1). However, the code samples might not always be in the `HEAD` revision, as I am writing and developing at the same time.

As development setup I'll be using PowerShell and VSCode with these extensions:

- [PureScript IDE](https://marketplace.visualstudio.com/items?itemName=nwolverson.ide-purescript)
- [PureScript Language support](https://marketplace.visualstudio.com/items?itemName=nwolverson.language-purescript)

## Running PureScript

In order to run PureScript, I installed `purescript` and `spago` in my global space:

```
yarn global add purescript
yarn global add spago
```

Then I created an empty project template with

```
spago init
```

This can be built and run with

```
spago build
spago run
```

It should output something like:

![](/purescript1.png "result")

## Making a web app

There are several options to create a web app in PureScript. A popular one seems to be [Halogen](https://github.com/purescript-halogen/purescript-halogen) but I decided to go with [flame](https://flame.asafe.dev/index), mostly because I liked the web site and the sample seemed straight forward. Also, I was interested in the Elm architecture.

Adding flame was easy

```
spago install flame
```

After that, I replaces `src/Main.hs` with the counter sample from the flame website (or from [here](https://github.com/easafe/purescript-flame/blob/master/examples/Counter/Counter.purs)), fixed the namespace to `Counter.Main` and added an `index.html` and `index.js` to my project:

`index.html`: 

```html
<!DOCTYPE html>
<html lang="en">
      <head>
            <meta charset="utf-8">
            <title>Counter Example</title>
      </head>
      <body>
	</body>
	<script type="module" src="index.js"></script>
</html
```

`index.js`:

```javascript
require('./output/Counter.Main').main();
```

In order to actually run the program, two things need to be done:

- Compile the PureScript code to Javascript output with `spago build`
- Run a bundler to produce a bundeled version of your app. For this part I'm using [parcel](https://parceljs.org/). You could also use [webpack](https://webpack.js.org/) or something else, but I have used webpack on several other occasions and wanted to check out parcel. Parcel is so-called zero-config, so I goes out and fetches all the dependencies to bundle by parsing `index.html` and then pulling the parts together. 

```
yarn init
yarn add -D parcel
yarn parcel index.html
```

Note that you have to have to add the `type="module"` to the `<script>` tag, otherwise parcel will complain.

This also starts a hot-reloading dev http server:

![](/purescript2.png "result")

## Adding styling

The sample above creates a very basic version of a web page. In order to make it more attractive, we need to add styled components. I was used to work with pre-fabricated components in frameworks (like the excellent [quasar framework](https://quasar.dev/) in Vue), but PureScript seems to require to use a CSS framework like [bootstrap](https://getbootstrap.com/), but I'm not a big bootstrap fan, so I'm trying out [bulma](https://bulma.io/) because you can't argue with 45k github stars.

All that needs to be done is to add 

```
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
```

to the `<head>` section of `index.html` and parcel will pick it up. You could also add the yarn package and do an `@import`, but I don't see any benefit in this at the moment.

After this you can decorate your elements with `HA.class'` with the CSS classes from bulma:

```
HE.button [HA.class' "button", HA.onClick Decrement] "-"
```